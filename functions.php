<?php

$DEBUG = false;

/**
 * Proper way to enqueue scripts and styles
 */
function winkel_storefront_enqueue_styles()
{
	/* Styles */
	wp_enqueue_style('bootstrap-style', get_stylesheet_directory_uri() . '/node_modules/bootstrap/dist/css/bootstrap.min.css');
	wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css', array(), fileatime(get_template_directory_uri() . '/style.css'), 'all');

	// $handle = 'opentickets-style';
	// $src = plugins_url() . '/opentickets-community-edition/templates/tickets/basic-style.css';
	// wp_enqueue_style($handle, $src, array(), fileatime($src), 'all');


	/* Scripts */
	// wp_enqueue_script('popper-script', get_stylesheet_directory_uri() . '/node_modules/jquery/dist/jquery.min.js');
	// wp_enqueue_script('jquery-script', get_stylesheet_directory_uri() . '/node_modules/popper.js/dist/popper.min.js');
	// wp_enqueue_script('alert-script', get_stylesheet_directory_uri() . '/assets/js/alert.js');
}
add_action('wp_enqueue_scripts', 'winkel_storefront_enqueue_styles');


if ($DEBUG) {
	echo get_template_directory_uri();
	echo '<br/>';
	echo get_stylesheet_directory_uri();
	echo '<br/>';
	echo get_stylesheet_uri();
	echo '<br/>';
	echo plugins_url() . '/opentickets-community-edition/templates/tickets/basic-style.css';
}

/** 
 * Change footer
 */
require 'inc/storefront-template-functions.php';
